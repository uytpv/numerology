ALTER TABLE `customers`
ADD `year` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `root` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `age` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `pinnacle` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `challennge` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `hidden_passion` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `subconscious_confidence` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `rational_thought` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `karmic_lessons` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `maturity` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `birthday` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `balance` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `hdp_bridge` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `personality` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `heart_desire` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `lpe_bridge` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `expression` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers`
ADD `life_path` CHAR(50) NOT NULL
AFTER `note`;
ALTER TABLE `customers` CHANGE `year` `year` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `root` `root` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `age` `age` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `pinnacle` `pinnacle` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `challennge` `challennge` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `hidden_passion` `hidden_passion` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `subconscious_confidence` `subconscious_confidence` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `rational_thought` `rational_thought` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `karmic_lessons` `karmic_lessons` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `maturity` `maturity` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `birthday` `birthday` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `balance` `balance` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `hdp_bridge` `hdp_bridge` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `personality` `personality` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `heart_desire` `heart_desire` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `lpe_bridge` `lpe_bridge` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `expression` `expression` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `life_path` `life_path` CHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `year` `year` CHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `customers` CHANGE `year` `year` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
