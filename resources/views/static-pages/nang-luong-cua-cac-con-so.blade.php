<?php
use App\Models\IndicatorNumber;
use App\Models\Indicator;
?>
@extends('layout.master')

@section('content')
    <section class="visual">
        <h2>Năng Lượng Các Con Số</h2>
        <div class="container area trans80">
            <div class="visual-list">
                <div class="text-holder pdlr10percent">
                    <h3>Ý NGHĨA CỦA THẦN SỐ HỌC SỐ 1</h3>
                    <p>Trong thần số học số 1 rất có cá tính, họ độc đáo và thường nổi bật giữa đám đông. Vì họ thường đi
                        những con đường ít ai lựa chọn, họ làm những thứ theo cách của riêng mình, và cùng có thể không tuân
                        theo lẽ thường.</p>

                    <p>Sự cá tính và độc đáo là nguồn sức mạnh lớn nhất của họ. Chúng cũng chính là chìa khóa mang họ đến
                        với thành công. Khi người số 1 đủ dũng cảm để khác biệt với đám đông và không cần sự tán thành từ
                        người khác, đó sẽ là lúc họ bắt đầu phát huy sức mạnh một cách tối đa và cải thiện rõ rệt cuộc sống
                        của mình.</p>

                    <p> Thần số học số 1 có thể tự thúc đẩy bản thân và dựa vào chính mình. Họ là những nhà lãnh đạo thiên
                        bẩm, không thích bị ra lệnh mà muốn là người chỉ huy. Nếu không được tự mình ra quyết định, họ có
                        thể sẽ hướng đến một vị trí có uy quyền hoặc đơn giản là làm việc một mình.</p>

                    <p>Nhiều người mang thần số học số 1 có một năng lực giám sát và điều hành, và thích làm công việc quản
                        trị hoặc ngành nghề một cách tự do. Họ thường bị hấp dẫn bởi các lĩnh vực nghề nghiệp đổi mới và
                        sáng tạo, mà ở đó họ có thể khởi xướng các dự án hoặc đảm nhận vai trò lãnh đạo.</p>
                    <p> Đối với tất cả chúng ta, cuộc đời là một hành trình khám phá bản thân, và đối với những người số 1
                        đây cũng chính là mục đích sống của họ. Nhận thức rõ bản thân, cùng với việc thấu hiểu mối liên hệ
                        giữa mình với mọi người, đây là lĩnh vực mà số 1 cần tập trung nhất trong cuộc sống.</p>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>Ý NGHĨA CỦA THẦN SỐ HỌC SỐ 2</h3>
                    <p> Thần số học số 2 là những người hòa giải, giỏi hợp tác, sở hữu năng lực thiên bẩm trong việc xoa dịu
                        và chữa lành con người, động vật và môi trường xung quanh họ. Mặc dù họ có thể sẽ tự ti và rụt rè
                        khi chưa cảm thấy thoải mái với ngoại cảnh, nhưng họ vẫn là những người đáng mến, chân thành và dễ
                        kết thân.</p>

                    <p>Họ là những nhà tư vấn tâm lý trời phú với sự thấu hiểu sâu sắc về người khác và những vấn đề của họ.
                        Đây là lý do vì sao họ thường đóng vai trò cố vấn trong cuộc sống riêng tư và trong công việc.
                        Người số 2 bẩm sinh thích hòa bình và sự đồng thuận, họ sẽ tránh không tranh cãi bằng bất cứ giá
                        nào, và thường vì thế mà hy sinh lợi ích của chính mình. Do đó học cách coi trọng bản thân và đặt
                        chính mình lên làm ưu tiên là một trong những bài học lớn nhất của cuộc đời họ.</p>

                    <p> Điều đó không dễ dàng với người số 2, nên họ thường cho đi nhiều hơn nhận lại. Họ có thể bị lợi dụng
                        hoặc chịu thiệt thòi. Một khi họ đủ can đảm dùng tiếng nói của bản thân và dám nói “không”, đó chính
                        là lúc họ bắt đầu phát huy sức mạnh một cách tối đa và cải thiện rõ rệt cuộc sống của mình.</p>

                    <p>Những người mang thần số học số 2 đầy nhạy cảm, trực giác tốt, đáng tin cậy, tận tụy và đầy tình cảm.
                        Họ là những nhà hòa giải hoặc người mang đến hòa bình, đôi khi có thể đưa đến khả năng tái cấu trúc
                        lại một tổ chức (và trong thời đại ý thức được đánh giá cao này, đây là một đức tính rất quý). Người
                        Số 2 là người ít bị Cái Tôi chi phối nhất, thể hiện sự nhạy bén và ít vị kỷ đáng quy trong những
                        trường hợp phải hy sinh bản ngã của mình để hòa nhập vào tinh thần chung khi cần.</p>
                    <p> Người Số 2 nên vận dụng trực giác mạnh mẽ của họ để phát triển tính tự tin và chọn lựa được những
                        người bạn hay đối tác làm việc nào có thể đánh giá cao những đặc thù của họ.
                        Điều này rất quan trọng ảnh hưởng đến con đường phát triển cá nhân của họ. Khi họ trưởng thành,
                        người Số 2 sẽ tự động phát hiện ra khả năng kiểm soát cảm xúc, học cách sử dụng nó như một công cụ
                        hỗ trợ để diễn đạt những biểu cảm tinh tế của mình.
                        Nó cũng sẽ rất có lợi cho họ để phát triển các lĩnh vực liên quan đến trí não, đặc biệt là khả năng
                        suy luận và trí nhớ. Sự phát triển theo hướng này sẽ vững chắc khẳng định lòng tự trọng của họ và
                        góp phần đưa đến hạnh phúc cá nhân tốt đẹp hơn nữa.
                        Người Số 2 nhạy cảm, đầy trực giác, năng giúp đỡ, đáng tin cậy, nhà hòa giải, đầy tình cảm và có máu
                        nghệ thuật.</p>
                </div>

            </div>
        </div>

    </section>


@endsection
