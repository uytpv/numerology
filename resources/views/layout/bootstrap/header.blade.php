<div class="container">
    <header class="d-flex justify-content-center py-3">
        <ul class="nav nav-pills">
            <li class="nav-item"><a href="{{ env('APP_URL') }}" class="nav-link active" aria-current="page">QUAY LẠI</a></li>
        </ul>
    </header>
</div>