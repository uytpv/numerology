<div class="box">
    <div class="box-body table-responsive no-padding">
        <div class="dd" id="t{{ $id }}">
            <ol class="dd-list">
                @each($branchView, $items, 'branch')
            </ol>
        </div>
    </div>
</div>