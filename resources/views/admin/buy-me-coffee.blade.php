<section class="content">
    <div class="error-page">
        <h2 class="headline text-yellow"><i class="fa fa-coffee text-yellow"></i> </h2>
        <div class="error-content" style="font-size: 1.2em">
            <h3>Buy Me a Coffee</h3>
            <p>Cảm ơn bạn đã sử dụng công cụ Numerology!</p>
            <p>Nếu bạn thấy công cụ này hữu ích, hãy giúp chúng tôi duy trì và phát triển nó bằng cách
                <br /> <b class="text-yellow">"BUY ME A COFFEE"</b>.
            </p>
            <p>Mỗi đóng góp của bạn sẽ giúp chúng tôi tiếp tục cung cấp dịch vụ miễn phí này cho cộng đồng.</p>
            <p>Hãy quét mã QR bên dưới để ủng hộ. Cảm ơn bạn rất nhiều!</p>
            <img class="img-responsive pad" src="{{ env('APP_URL') }}/img/qrcode-momo-Uy-10000.png" alt="Photo">
        </div>
    </div>
</section>

{{-- <div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">You like this tool?</h3>
            </div>

            <div class="box-body">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item next left" style="background-color: #636b6f;">
                            <img src="http://placehold.it/2200x700/39CCCC/ffffff&amp;text=Buy Me A Coffee"
                                alt="First slide">
                            <div class="carousel-caption">
                                First Slide
                            </div>
                        </div>
                        <div class="item active left">
                            <img src="http://placehold.it/2200x700/f39c12/ffffff&amp;text=Buy Me A Coffee"
                                alt="Third slide">
                            <div class="carousel-caption">
                                Third Slide
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                    </a>
                </div>
            </div>

        </div>

    </div>
</div> --}}
