<?php
use App\Models\IndicatorNumber;
use App\Models\Indicator;
?>


<?php $__env->startSection('content'); ?>
    <section class="visual">
        <h2>Chính Sách Quyền Riêng Tư</h2>
        <div class="container area trans80">
            <div class="visual-list">
                <div class="text-holder pdlr10percent">
                    <h3>1. Mục đích</h3>
                    <p>Chính sách quyền riêng tư này nhằm mục đích giải thích cách thức mà amunselect.com thu thập và sử
                        dụng thông tin cá nhân của người dùng.</p>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>2. Thông tin thu thập</h3>
                    <p>Amunselect.com có thể thu thập các loại thông tin cá nhân sau từ người dùng:</p>
                    <ul>
                        <li>Thông tin cá nhân được cung cấp bởi người dùng: Amunselect.com có thể thu thập thông tin cá nhân
                            từ người dùng khi họ tra cứu thông tin Năng lượng các con số. Thông tin cá nhân này có thể bao
                            gồm tên, và ngày sinh.</li>
                        <li>Thông tin được thu thập tự động: Amunselect.com có thể thu thập thông tin từ người dùng một cách
                            tự động, bao gồm thông tin về thiết bị, trình duyệt, hoạt động duyệt web, và địa chỉ IP.</li>
                    </ul>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>3. Cách sử dụng thông tin</h3>
                    <p>Amunselect.com sử dụng thông tin cá nhân thu thập được cho các mục đích sau:</p>
                    <ul>
                        <li>Cung cấp các dịch vụ và sản phẩm cho người dùng.</li>
                        <li>Liên hệ với người dùng.</li>
                        <li>Cải thiện chất lượng dịch vụ và sản phẩm.</li>
                        <li>Đưa ra các thông tin quảng cáo phù hợp với người dùng.</li>
                    </ul>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>4. Chia sẻ thông tin</h3>
                    <p>Amunselect.com có thể chia sẻ thông tin cá nhân của người dùng với các bên thứ ba sau:</p>
                    <ul>
                        <li>Các nhà cung cấp dịch vụ bên thứ ba: Amunselect.com có thể sử dụng các nhà cung cấp dịch vụ bên
                            thứ ba để hỗ trợ các hoạt động của mình, chẳng hạn như lưu trữ dữ liệu, xử lý thanh toán, và gửi
                            email. Các nhà cung cấp dịch vụ này có thể truy cập và sử dụng thông tin cá nhân của người dùng
                            để thực hiện các nhiệm vụ được giao cho họ.</li>
                        <li>Các cơ quan chính phủ: Amunselect.com có thể chia sẻ thông tin cá nhân của người dùng với các cơ
                            quan chính phủ nếu được yêu cầu theo quy định của pháp luật.</li>
                    </ul>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>5. Quyền của người dùng</h3>
                    <p>Người dùng có các quyền sau đối với thông tin cá nhân của mình:</p>
                    <ul>
                        <li>Quyền truy cập: Người dùng có quyền yêu cầu Amunselect.com cung cấp thông tin về thông tin cá
                            nhân của họ mà Amunselect.com đang lưu giữ.</li>
                        <li>Quyền sửa đổi: Người dùng có quyền yêu cầu Amunselect.com sửa đổi thông tin cá nhân của họ nếu
                            thông tin đó không chính xác hoặc không đầy đủ.</li>
                        <li>Quyền xóa: Người dùng có quyền yêu cầu Amunselect.com xóa thông tin cá nhân của họ.</li>
                        <li>Quyền từ chối tiếp thị: Người dùng có thể từ chối nhận thông tin tiếp thị từ Amunselect.com.</li>
                    </ul>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>6. Bảo mật thông tin</h3>
                    <p>Amunselect.com sử dụng các biện pháp bảo mật hợp lý để bảo vệ thông tin cá nhân của người dùng khỏi bị
                        truy cập trái phép, sử dụng hoặc tiết lộ.</p>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>7. Thay đổi chính sách</h3>
                    <p>Amunselect.com có thể thay đổi chính sách quyền riêng tư này bất cứ lúc nào. Bất kỳ thay đổi nào đối
                        với chính sách này sẽ được công bố trên trang này.</p>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>8. Liên hệ</h3>
                    <p>Nếu bạn có bất kỳ câu hỏi nào về chính sách quyền riêng tư này, vui lòng liên hệ với chúng tôi theo
                        thông tin liên hệ:</p>
                    <ul>
                        <li>Email: pigbanguy@gmail.com</li>
                    </ul>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>9. Đối tượng áp dụng</h3>
                    <p>Chính sách quyền riêng tư này áp dụng cho tất cả người dùng truy cập và sử dụng Amunselect.com.</p>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\UY\works\numerology\resources\views/static-pages/privacy.blade.php ENDPATH**/ ?>