<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thần Số Học - Map For Success</title>
    <meta name="description"
        content="A free responsive website template made exclusively for Frittt by Themesforce and Sarfraz Shaukat">
    <meta name="keywords" content="website template, css3, one page, bootstrap, app template, web app, start-up">
    <meta name="author" content="Themesforce and Sarfraz Shaukat for Frittt">
    <link rel="icon" type="image/png" href="<?php echo e(env('APP_URL'), false); ?>/vendor/free-css-sports/favicons/favicon-16x16.png"
        sizes="16x16">
    <link rel="stylesheet" href="<?php echo e(env('APP_URL'), false); ?>/vendor/free-css-sports/css/bootstrap.css">
    <link rel="stylesheet"
        href="<?php echo e(env('APP_URL'), false); ?>/vendor/free-css-sports/fonts/font-awesome-4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo e(env('APP_URL'), false); ?>/css/floating-hotline-with-number.css">

    <link rel="stylesheet" href="<?php echo e(env('APP_URL'), false); ?>/vendor/free-css-sports/css/all.css?v=20210805">
    <link rel="stylesheet" href="<?php echo e(env('APP_URL'), false); ?>/css/style.css?v=20210805">

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700|Source+Sans+Pro:400,700,400italic,700italic'
        rel='stylesheet' type='text/css'>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-Q97MK9PP27"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-Q97MK9PP27');
    </script>

<!-- Mã quảng cáo Google AdSense tài khoản ttn.nga2018@gmail.com -->
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3167317540432811"
crossorigin="anonymous"></script>
</head>
<?php /**PATH /home/stackops/www/numerology/resources/views/layout/head.blade.php ENDPATH**/ ?>