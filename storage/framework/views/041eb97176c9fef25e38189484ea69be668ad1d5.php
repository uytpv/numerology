<?php
use App\Models\IndicatorNumber;
use App\Models\Indicator;
?>


<?php $__env->startSection('content'); ?>
    <section class="visual">
        <h2>Thần Số Học Là Gì?</h2>
        <div class="container area trans80">
            <div class="visual-list">
                <div class="text-holder pdlr10percent">
                    <h3>Tìm hiểu về thần số học</h3>
                    <p>Thần số học là một môn khoa học tiên đoán và nhìn nhận về cuộc sống con người, thông qua những con số
                        đã xuất hiện cách đây 2500 năm. Nó được nghiên cứu và phát triển bởi nhà toán học, triết học vĩ đại
                        người Hy Lạp – Pythagoras.</p>

                    <p>Ông là người đầu tiên thấy được sức mạnh, cũng như năng lượng của các con số trong nền tảng của vũ
                        trụ, và mang chúng đến với nhân loại hiện nay. Vạn vật đều sở hữu một tần số năng lượng riêng – và
                        các con số cũng không ngoại lệ. Trên thực tế, mọi con số (cũng như chữ cái) đều có tần số năng lượng
                        riêng; những tần số năng lượng đó đều góp phần ảnh hưởng đến cuộc đời bạn.</p>

                    <p> Bởi vậy có thể nói chính xác, Thần số học là bộ môn nghiên cứu mối quan hệ giữa các con số và chữ
                        cái (trong họ tên, ngày sinh của bạn, trong tên công ty, doanh nghiệp, ngày khai trương…) với những
                        đặc điểm tính cách và những sự kiện, hành trình trong cuộc đời chúng ta. Nó là một môn khoa học siêu
                        hình, nó chỉ ra bản thiết kế rõ nét của cuộc đời bất cứ con người nào. Vì vậy Thần số học được coi
                        là một công cụ phát triển bản thân chuẩn xác, mạnh mẽ nhất hiện nay.</p>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>Thần số học bắt nguồn từ đâu?</h3>
                    <p>Có nhiều hệ thống Thần số học bắt nguồn từ những nền văn minh, thời đại và khu vực khác nhau trên
                        khắp thế giới. Theo các ghi chép, môn khoa học này xuất hiện từ hàng ngàn năm trước, thời đại của
                        các nền văn minh Atlantis, Babylon, Trung Quốc, Ai cập, Ấn Độ và Hy Lạp cổ đại. Trong đó hệ thống
                        Chaldea (do người Chaldea ở Babylon cổ đại phát triển) được coi là cổ xưa nhất. Tuy nhiên các nhà
                        Thần số học hiện đại đánh giá hệ thống này đã lỗi thời và không còn chuẩn xác đối với với thời điểm
                        hiện tại.</p>

                    <p>Ngày nay, Thần số học phương Tây (hay còn gọi là Thần số học hiện đại) là hệ thống chuẩn xác và được
                        sử dụng phổ biến hơn cả trên toàn thế giới. Nó được sáng tạo bởi triết gia và nhà toán học Hy Lạp
                        Pythagoras cách đây hơn 2500 năm. Ông được xem là cha đẻ của Thần số học hiện đại và là người khiến
                        cho nó được biết đến rộng rãi trên thế giới. Các hệ thống khác đang được sử dụng trên thế giới bao
                        gồm Thần số học Kabbal, Trung Quốc và Tamil/Ấn Độ</p>
                </div>
                <div class="text-holder pdlr10percent">
                    <h3>Có thể bạn chưa biết</h3>
                    <p>Nhà toán học Pythagoras chính là người phát minh ra “định lý Pytago” – một trong những định lý nổi
                        tiếng nhất trong toán học, định lý này mô tả quan hệ giữa ba cạnh của một tam giác vuông. Cho đến
                        nay định lý này vẫn được giảng dạy trong các trường cấp hai trên toàn thế giới.</p>

                    <p>Pythagoras rõ ràng là một chuyên gia trong lĩnh vực của mình, vậy nên nếu bạn ngờ vực về độ tin cậy
                        của Thần số học, thì tôi chỉ muốn hỏi bạn câu này: Nếu Pythagoras được coi là một thiên tài của thời
                        đại, lẽ nào ông lại sáng tạo ra một thứ vớ vẩn? Xin hết!</p>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\UY\works\numerology\resources\views/static-pages/than-so-hoc-la-gi.blade.php ENDPATH**/ ?>