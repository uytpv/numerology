<?php
use App\Models\IndicatorNumber;
use App\Models\Indicator;
?>


<?php $__env->startSection('content'); ?>
    <section class="visual">
        <h2>Các Chỉ Số</h2>
        <div class="container area trans80">
            <div class="visual-list">
                <div class="text-holder pdlr10percent">
                    <h3>Chỉ Số Dường Dời</h3>
                    <p>Chỉ số đường đời là chỉ số quan trọng nhất.</p>


                </div>
                <div class="text-holder pdlr10percent">
                    <h3>Chỉ Sô Sứ Mệnh</h3>
                    <p>Chỉ số sứ mệnh là chỉ số quan trọng thứ 2 trong bản đồ thần số học của bạn. Nó còn có những tên khác
                        như Chỉ số khả năng hay Chỉ số tên. Nó được tính dựa trên tên đầy đủ của bạn trong giấy khai sinh.
                    </p>

                    <p>Chỉ số này tiết lộ bạn sinh ra để làm gì và trở thành người như thế nào trong cuộc đời này. Nó cho
                        biết những năng lực nội tại trong bạn mà bạn phải sử dụng trong suốt hành trình của cuộc đời. Cùng
                        với Chỉ số Đường đời, đây cũng là một chỉ số mà bạn cần xét đến khi lựa chọn nghề nghiệp.</p>
                </div>

            </div>
        </div>

    </section>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/stackops/www/numerology/resources/views/static-pages/cac-chi-so.blade.php ENDPATH**/ ?>